Opstelling in (event-)ruimte om doorloop in ruimte te meten. Weergeven op mogelijk andere pi met scherm of door laptop..
Use-cases zijn:
- [ ] WC Status (qua layout wat lastig en daarbij misschien beetje ongepast, qua verzamalen data over duur van WC bezoek..)
- [ ] Inloop launch event (simpele teller met fysiek boompje a la ski wedstrijd poortje, zowel ingaand als uitgaand)
- [x] Bezoek demo ruimte zelfde als hierboven, maar dan beperkt tot demo-ruimte. Te versimpelen door afstand meter op schouderhoogte te zetten en dalen tussen pieken te tellen.

De laatste opstelling hebben we gebruikt tijdens het event. Te beginnen bij het door de leverancier geleverde voorbeeld script. Door dit script telkens aan te passen gedurende het launch-event werd dit een voorbeeld in de praktijk van het testen en doorontwikkelen van een simpele applicatie. In een klant workshop sessie zou dit ook kunnen werken, ook bijvoorbeeld als onderdeel van backlog bepaling, werking van CI/CD.

Mijn voorstel is om dit uit te werken als een variant waarin we: 
1. weer beginnen met basis script,
2. testen of het werkt en wat er gebeurt
3. aanpassing doen in script, bijvoorbeeld het logo veranderen
4. bedenken hoe je de meeting inzet voor telling van bepaalde handelingen
5. aanpassing doen van script en testen of het werkt.
6. Pipeline introduceren om wijzigingen centraal bij te houden en pull te doen vanaf Pi (bijvoorbeeld in GitLab)
7. uitleg git en gitlab gebruik over: Master, branches, clone, pull, add, commit, push, merge en pull requests
8. uitleg over gitlab om Issues bij te houden, je backlog etc.
 


Use-case uitbreiden naar afbeelden van sensor data op andere Pi web server of cloud omgeving, eventueel ook data analyse tooling voor een doorkijk naar een andere workshop, over hoe je time-series data gebruikt en hoe je verschillende gegevensbronnen aan elkaar koppelt.