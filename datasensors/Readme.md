Outline: Raspberry Pi met sensor genereert time series data welke in Google cloud geanalyseerd kan worden.
```mermaid
sequenceDiagram
    participant Sensor
    participant Raspberry Pi
    participant Google PubSub
    participant Google BigTable
    participant Google DataStudio
    Sensor->Raspberry Pi: Sensor Data
    Raspberry Pi->>Google PubSub: MQTT over Internet
    Google PubSub->>Google BigTable: Data to store
    Google BigTable->>Google DataStudio: Data to notebook
```

Wat heb je nodig:
- [x] Raspberry Pi, USB voeding (5V, 2A) of batterij met voldoende capaciteit voor mobiele toepassing en (draadloos) netwerk, voor Internet verbinding.
- [x] Laptop of PC voor configuratie van de SD kaartje met besturingssysteem voor Raspberry Pi, werken in Google Cloud en op afstand configureren van de Raspberry Pi.
- [x] Bij Raspberry Pi passende SD-kaartje met Raspbian OS erop (zie korte handleiding Balena Etcher).
- [x] Breadboard of andere manier om sensor aan te sluiten, in deze beschrijving gebruik ik de Pimoroni Breakout Garden en bijbehorende sensoren.
- [x] Een scherm met HDMI verbinding, toetsenbord en muis om handelingen op je Pi uit te voeren, maar voor het overgrote deel van de tutorial ga ik uit het configureren van SD kaart op een laptop en op afstand configureren van de Pi.

Bij sensoren heb je verschillende keuzes: licht, temperatuur, vochtigheid, luchtdruk, hoogte, beweging, afstand, hartslag... Bedenk wat je wil meten en bedenk of deze meetgegevens geschikt zijn voor zogenaamde Time Series data, waarbij over tijd gemeten er genoeg verandering plaatsvind om analyse op los te laten. Dus in een dag-demo setting is misschien temperatuur niet de meest spannende, maar zijn bijvoorbeeld een beweging, licht, hartslag en/of afstand sensoren wel geschikt. Er kan altijd voorafgaand aan een workshop/demo data over langere tijd verzameld worden, denk dan aan binnen temperatuur, CO2 ontwikkeling en luchtvochtigheid, bijvoorbeeld in relatie tot meteo statistieken en/of energie verbruik over dezelfde periode. Voor de demo zal ik zowel actuele voorbeelden tijdens event gemeten als eerder verzamelde gegevens gebruiken, om analyse in Google mee te demonstreren.

## Inrichten van PC
Om de eenvoudig de tutorials te volgen heb je de volgende software nodig op de PC waar vandaan je de verschillende configuraties uitvoert.
1. Download en installeer gitbash voor Windows (er zijn ook Microsoft eigen alternatieven zoals WSL, maar deze zijn voor deze tutorial aanpak te complex qua inrichting en aansluiting op de te installeren tooling).
2. Controleer Mac/Linux installaties op aanwezigheid van git/openssh-client.
3. Download en installeer Visual Studio Code (Windows/Mac/Linux), of gebruik je eigen software IDE naar keuze.

Zorg dat je een account hebt op Gitlab, dit account kan gebruikt worden om via Git, Gitbash de benodigde configuratie en software code bestanden op je PC te krijgen om daar te bewerken. Als alternatief kan de bestanden ook handmatig downloaden en bewerken. 

### Rapsberry Pi Setup
Kijk voor instructies op de Wiki:
[Raspberry Pi Setup](https://gitlab.com/quintgroup/launch-event-examples/wikis/Raspberry-Pi-Setup)

### Pi Sensor aansluiting en configuratie
Voor deze demo gebruiken we de Breakout Garden van Pimoroni, deze is speciaal voor de Pi gemaakt en past precies bovenop de Pi. Ook zijn er pasklare sensoren voor beschikbaar, met goed gedocumenteerde Python bibliotheek voorbeelden om de sensoren uit te lezen. Dit vereenvoudigt het verzamelen van gegevens aanzienlijk en maakt het eenvoudig om de gegevens up te loaden naar Google.

Zodra je weer ingelogd bent op je Pi (via ssh of direct op de Pi zelf), volg ga dan naar deze Gitlab pagina vanaf je Pi. Bijna alle stappen vinden plaats op de Pi zelf en deze pagina maakt knippen en plakken eenvoudig (o.a. via Git synchroniseren van de configuratie bestanden en scripts).

Om direct aan de slag te kunnen met de sensoren gebruiken we de volgende git repositories die je op de volgende locaties op kan halen.
Open op de Raspberry Pi de LXTerminal (bovenin herkenbaar aan de ">_") en knip en plak (1-voor-1) de onderstaande regels:

`git clone https://gitlab.com/quintgroup/launch-event-examples.git`

`git clone https://github.com/quintest/breakout-garden.git`

Zodra je deze repositories gekloned hebt ga je met het volgende commando:

`cd ~/breakout-garden/`

`sudo ./install.sh`

Volg de aanwijzingen op het scherm (druk op Enter), hiermee detecteert het script welke sensoren je in de Breakout Garden hebt geplaatst en installeert het script de juiste drivers.

Om te testen of de sensor werkt kies je uit de map Examples een voorbeeld programma, deze eindigen op .py. Kijk eerst even naar het script, daar staat in de beschrijving ook welke sensoren je nodig hebt. Het uitvoeren van een script doe je door met het "cd" commando naar de locatie examples te navigeren en je voert het programma uit door volgende in te typen op de Pi, vul voor <script_naam> de naam van het script in:

`./<script_naam>.py`

## Google Cloud, IoT en DataStudio
De Google IoT omgeving is gericht op het controleren en veilig verbinden van geauthoriseerde devices, waardoor het eenvoudiger wordt gegevens te transporteren en deze in te zetten voor verdere analyse en of monitoring doeleinden. Vergelijkbare diensten kan je ook afnemen bij Amazon, MS Azure en een aantal gespecialiseerde IoT Cloud platformen (waaronder ook Balena.io).
- [Inrichten Google Cloud IoT omgeving](https://cloud.google.com/solutions/iot-overview)
- [Inrichten Pi Google IoT](https://github.com/ARM-software/Cloud-IoT-Core-Kit-Examples#enabling-cloud-iot-core-ap-installing-the-google-cloud-sdk-and-registering-your-first-device)

Middels MQTT als lichtegewicht transport protocol zal de Raspberry Pi de sensor data kunnen versturen richting de Google PubSub dienst, welke de data overbrengt naar de juiste opslag, in het geval van Google Cloud is dat voor Time Series data bij voorkeur Google Big Table. Daarna kan de data in de Big Table gebruikt worden bij analyse met behulp van bijvoorbeeld DataStudio (een Python Notebook implementatie in de Google Cloud). Als alternatief kan ook Datalab gebruikt worden, al is dat misschien minder laagdrempelig qua inrichting en leercurve.

