# launch-event-examples

Demo setups voor bij launchevent Springstof. Voeg je voorstellen toe aan deze Readme.md door op Readme.md te klikken en dan in het volgende venster op Edit te klikken. Daarna Commit je de changes met de groene button onderaan.

## Voorstellen
Zet hieronder je voorstel titel, korte beschrijving en naam als een bullet (zet een Asterix voor de zin en je krijgt een bullet). De projecten hoeven geen software code te bevatten, het kunnen ook beschrijvingen zijn van het project. Dit kan dan uiteindelijk fungeren als handleiding om het project ook door iemand anders uitvoerbaar te maken. Naast opname in de lijst kan je ook je idee als issue opvoeren, hiermee kunnen anderen aan je project meewerken en is de voortgang zichtbaar gemaakt. Zie voor meer info hierover de Wiki.

* [datasensors](/datasensors/Readme.md), data genereren obv Pi sensor opstelling en deze analyseren/presenteren via google cloud, @markuskeuter
* [chatbot](/Chatbot/readme), voorbeeld chatbot die je kunt aanpassen. @a.coenen
* [muziek](/algoritmische muziek/readme), muziek die met algoritmes wordt gegenereerd. @a.coenen
* [ruimte sensor](omgeving-sensor-ruimte), meetingen in ruimte om beweging vast te leggen @markuskeuter